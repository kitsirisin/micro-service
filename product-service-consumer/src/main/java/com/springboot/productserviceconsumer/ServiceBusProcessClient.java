package com.springboot.productserviceconsumer;

import com.azure.messaging.servicebus.*;
import com.google.gson.Gson;
import com.springboot.productserviceconsumer.dto.Product;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.Disposable;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
@RestController
@RequestMapping("/receive")
public class ServiceBusProcessClient {
    static String connectionString = "Endpoint=sb://esavingsbns.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=CrRyYCnmamAjvqiedWzh2il3lgDGyu+mkjMNOD7hHHA=";
    static String queueName = "product-queue";
    private static Product product;

    @Autowired
    private ProductServiceProxy proxy;

    @GetMapping("/test")
    public List<Product> test() {
        return proxy.getProducts();
    }

    @GetMapping("/receiveMessages")
    public String receiveSimple() throws InterruptedException {
        AtomicBoolean sampleSuccessful = new AtomicBoolean(true);
        CountDownLatch countdownLatch = new CountDownLatch(1);

        // The connection string value can be obtained by:
        // 1. Going to your Service Bus namespace in Azure Portal.
        // 2. Go to "Shared access policies"
        // 3. Copy the connection string for the "RootManageSharedAccessKey" policy.
        // The 'connectionString' format is shown below.
        // 1. "Endpoint={fully-qualified-namespace};SharedAccessKeyName={policy-name};SharedAccessKey={key}"
        // 2. "<<fully-qualified-namespace>>" will look similar to "{your-namespace}.servicebus.windows.net"
        // 3. "queueName" will be the name of the Service Bus queue instance you created
        //    inside the Service Bus namespace.

        // Create a receiver.
        ServiceBusReceiverAsyncClient receiver = new ServiceBusClientBuilder()
                .connectionString(connectionString)
                .receiver()
                .queueName(queueName)
                .buildAsyncClient();

        Disposable subscription = receiver.receiveMessages().subscribe(message -> {
                    // Process message. If an exception is thrown from this consumer, the message is abandoned.
                    // Otherwise, it is completed.
                    // Automatic message settlement can be disabled via disableAutoComplete() when creating the receiver
                    // client. Consequently, messages have to be manually settled.
                    System.out.printf("Sequence #: %s. Contents: %s%n", message.getSequenceNumber(), message.getBody());
                    String strJson = new String(String.valueOf(message.getBody()));
                    System.out.println("strJson = " + strJson);
                    Gson g = new Gson();
                    Product product = g.fromJson(strJson, Product.class);
                    System.out.println("product = " + product.toString());
                    proxy.saveProduct(product);
                },
                error -> {
                    System.err.println("Error occurred while receiving message: " + error);
                    sampleSuccessful.set(false);
                },
                () -> System.out.println("Receiving complete."));

        // Receiving messages from the queue for a duration of 20 seconds.
        // Subscribe is not a blocking call so we wait here so the program does not end.
/*        countdownLatch.await(20, TimeUnit.SECONDS);

        // Disposing of the subscription will cancel the receive() operation.
        subscription.dispose();

        // Close the receiver.
        receiver.close();

        // This assertion is to ensure that samples are working. Users should remove this.
        Assertions.assertThat(sampleSuccessful.get());*/
        return "finish";
    }


}
