package com.springboot.productserviceproducer;

import com.springboot.productserviceproducer.dto.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "product-service", url = "http://localhost:8091/product")
public interface ProductServiceProxy {
    @PostMapping("/saveProduct")
    public Product saveProduct(@RequestBody Product product);
    @GetMapping("/getProducts")
    public List<Product> getProducts() ;
}
