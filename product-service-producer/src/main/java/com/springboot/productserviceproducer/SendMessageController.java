package com.springboot.productserviceproducer;

import com.azure.messaging.servicebus.ServiceBusClientBuilder;
import com.azure.messaging.servicebus.ServiceBusMessage;
import com.azure.messaging.servicebus.ServiceBusSenderClient;
import com.google.gson.Gson;
import com.springboot.productserviceproducer.dto.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
public class SendMessageController {

    static String connectionString = "Endpoint=sb://esavingsbns.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=CrRyYCnmamAjvqiedWzh2il3lgDGyu+mkjMNOD7hHHA=";
    static String queueName = "product-queue";

    @PostMapping("/saveProduct")
    public String saveProduct(@RequestBody Product product){
//        rabbitMqSender.send(str);
        // create a Service Bus Sender client for the queue
        ServiceBusSenderClient senderClient = new ServiceBusClientBuilder()
                .connectionString(connectionString)
                .sender()
                .queueName(queueName)
                .buildClient();

        // send one message to the queue
        senderClient.sendMessage(new ServiceBusMessage(new Gson().toJson(product)));
        System.out.println("Sent a single message to the queue: " + queueName);
        return "send Successfully";
    }
    @PostMapping("/messages")
    public String sendProduct(@RequestBody String str){
//        rabbitMqSender.send(str);
        // create a Service Bus Sender client for the queue
        ServiceBusSenderClient senderClient = new ServiceBusClientBuilder()
                .connectionString(connectionString)
                .sender()
                .queueName(queueName)
                .buildClient();

        // send one message to the queue
        senderClient.sendMessage(new ServiceBusMessage(str));
        System.out.println("Sent a single message to the queue: " + queueName);
        return "send Successfully";
    }
    @GetMapping("/messages")
    public String getMessages(@RequestParam String str){
        return str;
    }
}
