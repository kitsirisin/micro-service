package com.springboot.productserviceproducer.dto;
import lombok.Data;

@Data

public class Product {
    private String id;
    private String name;
    private Integer items;
    private String status;
    private String createBy;
    private String updateBy;

}
